import math
import datetime
import numpy as np
import pandas as pd
from operator import add
import tensorflow as tf
import matplotlib.pyplot as plt

# Path where the data is stored
DATA_PATH = './Data/'
# Model path
MODEL_PATH = './MODEL/WDS_Q0'
# STATE space dimension = (tank1_level_t, water_consumption , time of day (t), month, last_action, time_running, water quality)
# where tank level and demand of water at time t (in the format of ms) are continuous values; month and
# last action are one-hot encode, time_running discrete, and water quality binary.
STATE_SPACE = 25
# Size of expanded state
STATE_SIZE = 4
# Data that will be loaded to run simulations
YEAR = '2012'
MONTHS = ['Januar', 'Februar', 'Maerz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
MONTHSL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

# One-hot encoding for Months
MONTH_ENCODING = [[1,0,0,0,0,0,0,0,0,0,0,0], 
				  [0,1,0,0,0,0,0,0,0,0,0,0],
				  [0,0,1,0,0,0,0,0,0,0,0,0],
				  [0,0,0,1,0,0,0,0,0,0,0,0], 
				  [0,0,0,0,1,0,0,0,0,0,0,0],
				  [0,0,0,0,0,1,0,0,0,0,0,0],
				  [0,0,0,0,0,0,1,0,0,0,0,0],
				  [0,0,0,0,0,0,0,1,0,0,0,0],
				  [0,0,0,0,0,0,0,0,1,0,0,0],
				  [0,0,0,0,0,0,0,0,0,1,0,0],
				  [0,0,0,0,0,0,0,0,0,0,1,0],
				  [0,0,0,0,0,0,0,0,0,0,0,1]]

# Actions available
ACTIONS = [[0,0,0,0], [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]]
# To normalize the states data
MIN_TK1 = 47
MAX_TK1 = 57
MIN_CONSUMPTION = 1
MAX_CONSUMPTION = 3240

class Evaluation:

	def get_model(self):

		self.model = tf.keras.models.load_model(MODEL_PATH)

	def evaluate(self):	

		# Files to store the evaluation results
		f1 = open("results_evaluation.txt", "w")
		f2 = open("actions.txt", "w")
		#---------------------------------------------------------------------------------------------#
		############################ VARIABLES OF THE WATER SYSTEM MODEL ##############################
		#---------------------------------------------------------------------------------------------#

		Q1 = [0.0, 320.0, 570.0, 670.0, 897.0, 1099.0, 1298.0, 1597.3, 1823.2]
		H1 = [76.81, 74.70, 71.61, 71.75, 72.29, 69.99, 67.95, 61.88, 55.79]
		ETA1 = [0.00, 38.60, 55.55, 60.68, 72.28, 78.85, 83.12, 85.96, 85.07]

		Q2 = [0.0, 240.0, 494.0, 694.0, 899.0, 1095.7, 1209.3, 1465.3]
		H2 = [74.15, 72.15, 70.78, 69.34, 66.44, 62.75, 60.12, 52.57]
		ETA2 = [0.00, 40.65, 63.22, 75.36, 83.02, 87.09, 88.32, 87.11]

		Q3 = [0.0, 237.0, 470.0, 645.0, 797.3, 1000.3, 1208.7, 1410.0] 
		H3 = [63.84, 62.46, 61.98, 60.61, 58.68, 54.87, 50.07, 43.83]
		ETA3 = [0.00, 38.28, 61.02, 72.36, 79.05, 84.94, 87.08, 84.91]

		Q4 = [0.0, 195.0, 440.0, 659.7, 806.3, 998.7, 1204.0]
		H4 = [64.13, 63.19, 61.62, 59.35, 56.61, 51.21, 43.71]
		ETA4 = [0.00, 41.95, 67.50, 80.96, 84.84, 86.04, 83.42]

		A = math.pi*math.pow(44,2)/4;
		rho = 1000
		g = 9.81
		p1 = -2.713E-09
		p2 = 0.000006504
		p3 = 4.384E-07
		p4 = -0.0001768

		Qanlage = np.linspace(0,2000,2000)
		p = np.polyfit(Q1,H1,2)
		H1 = np.polyval(p,Qanlage)
		pETA1 = np.polyfit(Q1,ETA1,2)
		ETA1 = np.polyval(pETA1,Qanlage)
		p = np.polyfit(Q2,H2,2)
		H2 = np.polyval(p,Qanlage)
		pETA2 = np.polyfit(Q2,ETA2,2)
		ETA2 = np.polyval(pETA2,Qanlage)
		p = np.polyfit(Q3,H3,2)
		H3 = np.polyval(p,Qanlage)
		pETA3 = np.polyfit(Q3,ETA3,2)
		ETA3 = np.polyval(pETA3,Qanlage)
		p = np.polyfit(Q4,H4,2)
		H4 = np.polyval(p,Qanlage)
		pETA4 = np.polyfit(Q4,ETA4,2)
		ETA4 = np.polyval(pETA4,Qanlage)

		###############################################################################################

		# Store average consume daily by month
		average_electricity_month_np1 = np.zeros(12)
		average_electricity_month_np2 = np.zeros(12)
		average_electricity_month_np3 = np.zeros(12)
		average_electricity_month_np4 = np.zeros(12)
		average_electricity_month = np.zeros(12)

		# Store average cycles daily by month
		average_cycles_month_np1 = np.zeros(12)
		average_cycles_month_np2 = np.zeros(12)
		average_cycles_month_np3 = np.zeros(12)
		average_cycles_month_np4 = np.zeros(12)

		# Store average tank level
		average_tank_level = np.zeros((24,12))

		# Tank level
		HB = []
		# Tank level of the first timestep in real data 2012
		HB.append(53.42)

		# Store the current state that will be introduced in the model for predictions
		self.current_state = np.zeros((1, STATE_SIZE, STATE_SPACE))
		self.expanded_state = []

		''' ** For pump operation control '''
		# Time Running is the time that some pump is operating; last action was the last pump chosed
		time_running = [0, 0, 0, 0, 0]
		action_index = 2
		last_action = 2
		# To avoid often switches
		pump_switch = 0

		# Water quality initial value
		water_quality = 0

		# Cumulative Reward
		R = []
		cumulative_reward = 0

		total_consume = 0

		for indexm, month in enumerate(MONTHS):
			# Number of days in the month
			days_counter = 0

			# Minutes counter
			minutes_counter = 0

			# Consume of electricity
			consume_np1 = 0
			consume_np2 = 0
			consume_np3 = 0
			consume_np4 = 0

			# Number of switches in the settings
			switches_np1 = 0
			switches_np2 = 0
			switches_np3 = 0
			switches_np4 = 0

			# Tank level sum
			tk1_level = 0

			# Open dataframes
			data_time = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
			data_date = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['Date']]
			data_waterConsumption = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['Netzverbrauch_pval']]

			# Fix duplicated data in Water Consumption
			data_time_waterConsumption = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
			for i, d in data_time_waterConsumption.iterrows():
				h,m,s = data_time_waterConsumption.iloc[i, 0].split(':')
				if i == 0:
					h_i,m_i,s_i = data_time_waterConsumption.iloc[i, 0].split(':')
				elif int(m) == 0:
					if h_i == h:
						j = 0
						while h_i == h:
							data_time.drop([i + j], inplace=True)
							data_date.drop([i + j], inplace=True)
							data_waterConsumption.drop([i + j], inplace=True)
							j += 1
							h,m,s = data_time_waterConsumption.iloc[i + j, 0].split(':')
					else:
						h_i = h

			data_time.reset_index(drop=True, inplace=True)
			data_date.reset_index(drop=True, inplace=True)
			data_waterConsumption.reset_index(drop=True, inplace=True)					
			
			# Timesteps (remember that there's one day missing in NP3, that's why the length)
			data_length = len(data_waterConsumption)

			# Variables related to flow, head and electricity
			QBP= np.zeros(data_length)
			HBP = np.zeros(data_length)
			etaBP = np.ones(data_length)

			Hanlage = []

			for i in range(data_length):
				if(pd.isnull(data_waterConsumption.iloc[i, 0]) or pd.isna(data_waterConsumption.iloc[i, 0]) or data_waterConsumption.iloc[i, 0] <= 0):
					data_waterConsumption.iloc[i, 0] = last_valid_waterConsumption		
				else:
					last_valid_waterConsumption = data_waterConsumption.iloc[i, 0]
			
				# Count the number of days by month	
				if i == 0 and indexm == 0:
					#d,m,y = data_date.iloc[i, 0].split('.')
					h,m,s = data_time.iloc[i, 0].split(':')
					h_i = h
					#d_i = d	
				#else:
					#d_i,m,y = data_date.iloc[i, 0].split('.')

				# Reset the tank levels	
				if i == 0:
				# Reset the list of the tank levels
					HB_temp = HB[len(HB) - 1]
					HB = []
					HB.append(HB_temp)	

				h,m,s = data_time.iloc[i, 0].split(':')
				data_time.iloc[i, 0] = int(datetime.timedelta(hours=int(h),minutes=int(m),seconds=int(s)).total_seconds())

				aa = p1 * data_waterConsumption.iloc[i, 0] + p2
				bb = p3 * data_waterConsumption.iloc[i, 0] + p4
				cc = HB[i]

				Qanlage_1 = list(map(lambda x:aa*pow(x,2),Qanlage))
				Qanlage_2 = list(map(lambda x:bb * x + cc,Qanlage))
				Hanlage.append(list(map(add, Qanlage_1, Qanlage_2)))

				# Normalize data for the state
				tank_level = (HB[i] - MIN_TK1)/(MAX_TK1 - MIN_TK1)
				water_consumption =  (data_waterConsumption.iloc[i, 0] - MIN_CONSUMPTION)/(MAX_CONSUMPTION - MIN_CONSUMPTION)
				current_time = (data_time.iloc[i, 0] - 59)/(86399 - 59)
				time_running_normalized = [time/1440 for time in time_running]

				# Create a new state
				state = [tank_level, water_consumption, current_time] + MONTH_ENCODING[indexm]+ ACTIONS[last_action] + time_running_normalized + [water_quality]
				self.expanded_state.append(state)
				
				if len(self.expanded_state) < STATE_SIZE:
					action_index = last_action
				elif len(self.expanded_state) == STATE_SIZE:
					for j in range(STATE_SIZE):
						self.current_state[0][j] = self.expanded_state[j] 
					action_index = self.get_action()
				else:
					self.expanded_state.pop(0)
					for j in range(STATE_SIZE):
						self.current_state[0][j] = self.expanded_state[j]
					action_index = 	self.get_action()

				# Measure implemented to avoid impratical switches, so the system always wait 5 minutes before one change	
				if action_index != last_action and pump_switch < 4:
					pump_switch += 1
					action_index = last_action
				else: pump_switch = 0

				# Store the action index to count occurrences
				f2.write(str(action_index)+'\n')	

				if action_index > 0:
					if action_index == 1:
						HPumpe = H1
						ETAPumpe = pETA1
					elif action_index == 2:
						HPumpe = H2
						ETAPumpe = pETA2
					elif action_index == 3:
						HPumpe = H3
						ETAPumpe = pETA3
					elif action_index == 4:
						HPumpe = H4
						ETAPumpe = pETA4

					res = np.argmin(abs(Hanlage[i]-HPumpe))	

					QBP[i] = Qanlage[res]
					HBP[i] = HPumpe[res]
					etaBP[i] = np.polyval(ETAPumpe, QBP[i])
				else: 
					QBP[i] = 0
					HBP[i] = 0
					etaBP[i] = 1 

				# Tank level	
				#HB.append(HB[i]+(QBP[i]-data_waterConsumption.iloc[i, 0])/60/A)
				HB_i = 	HB[i]+(QBP[i]-data_waterConsumption.iloc[i, 0])/60/A
				if HB_i < 47:
					HB.append(47)
				elif HB_i > 57:
					HB.append(57)
				else:
					HB.append(HB_i)
				print('Tank level: '+str(HB[i+1]))
				print('Time running: '+str(time_running[action_index])+'\n')

				###############################################################################################
				######################################### EVALUATIONS #########################################	

				''' ELECTRICITY CONSUMPTION EVALUATION '''
				pbp = ((rho*g*QBP[i]/3600*HBP[i])/55*etaBP[i])/1000

				if action_index == 1:
					consume_np1 += pbp
				elif action_index == 2:
					consume_np2 += pbp
				elif action_index == 3:
					consume_np3 += pbp
				elif action_index == 4:
					consume_np4 += pbp
				total_consume += pbp

				''' PUMP SWITCHES EVALUATION '''
				# Count 1 to ON + 1 to OFF at once
				if action_index != last_action:
					if action_index == 1:
						switches_np1 += 2
					elif action_index == 2:
						switches_np2 += 2
					elif action_index == 3:
						switches_np3 += 2
					elif action_index == 4:
						switches_np4 += 2

				''' TANK LEVEL EVALUATION '''
				# Check if the hour have changed
				if(minutes_counter < 59):
					minutes_counter += 1
					tk1_level += HB[i]
				# Group the average consume by hour					
				elif(h_i != h or minutes_counter >= 59):
					average_tank_level[int(h), indexm] += tk1_level/minutes_counter
					tk1_level = 0
					minutes_counter = 0
					h_i = h

				# Update the time running and last action 
				#if(action_index != 0):
				time_running[action_index] += 1	

				###############################################################################################
				####################################### REWARD FUNCTION #######################################	

				# Check the previous action and the current action, update and give a penalty if the case
				if(action_index == last_action or action_index == 0 or time_running[action_index] == 0):
				#if action_index == last_action or time_running[action_index] == 0:
					p = 1 	
				else:
					p = 30	

				p += time_running[action_index]

				# Update the last action performed
				last_action = action_index

				# Check the constraints of the tank level and give a penalty if the case
				if HB_i < 50:
					c = abs(HB_i - 50)
				elif HB_i >= 57:
					c = 1
				else:
					c = 0

				# Clip the penalty value
				if c > 1 : c = 1

				# Check if water tank level decrease below to determined level for water quality
				if water_quality == 0 and HB[i] >= 50 and HB[i] < 53:
					c = -1
					water_quality = 1

				# *** Reward Function Q/Kw *** #	
				if action_index > 0:
					reward = math.exp(1/(-QBP[i]/etaBP[i])) - c*10 + math.log(1/p)
				else:
					reward = -c*10 + math.log(1/p)

				# Clip the reward value
				reward = (reward - (-18))/(11 - (-18))

				# *** Reward Function Kw *** #
				# if action_index > 0:
				# 	reward = -math.exp(-1/etaBP[i]) - c*10 + math.log(1/p)
				# else:
				# 	reward = -c*10 + math.log(1/p)	
				#Clip the reward value
				#reward = (reward - (-20))/(10 - (-20))
				
				cumulative_reward += reward

				if data_time.iloc[i, 0] == 86399:
					days_counter += 1
					#d = d_i
					# If it's a new day, reset the time operating counter	
					time_running = [0, 0, 0 ,0 ,0]
					# And reset the water quality control
					water_quality = 0
					# Store the cumulative reward for the episode
					R.append(cumulative_reward)
					cumulative_reward = 0
				
				###############################################################################################

			# Store the averages	
			average_electricity_month_np1[indexm] = consume_np1/days_counter
			average_electricity_month_np2[indexm] = consume_np2/days_counter
			average_electricity_month_np3[indexm] = consume_np3/days_counter
			average_electricity_month_np4[indexm] = consume_np4/days_counter
			#average_electricity_month[indexm] = (consume_np1+consume_np2+consume_np3+consume_np4)/days_counter

			average_cycles_month_np1[indexm] = switches_np1/days_counter
			average_cycles_month_np2[indexm] = switches_np2/days_counter
			average_cycles_month_np3[indexm] = switches_np3/days_counter
			average_cycles_month_np4[indexm] = switches_np4/days_counter

			average_tank_level[:, indexm] /= days_counter
			# To correct missing data in Maerz
			if MONTHS[indexm] == 'Maerz':
				average_tank_level[2, 2] *= days_counter
				average_tank_level[2, 2] /= (days_counter - 1)

		f1.write("########################################### PLOTS ########################################### \n")
		
		# Plot electricity consumption	
		y1 = average_electricity_month_np1
		y2 = average_electricity_month_np2
		y3 = average_electricity_month_np3
		y4 = average_electricity_month_np4

		f1.write('NP1: '+str(y1)+'\n')
		f1.write('NP2: '+str(y2)+'\n')
		f1.write('NP3: '+str(y3)+'\n')
		f1.write('NP4: '+str(y4)+'\n')
		f1.write('TOTAL: '+str(total_consume)+'\n')	
	
		# Plot the points  
		plt.plot(MONTHSL, y1, label='NP1')
		plt.plot(MONTHSL, y2, label='NP2')
		plt.plot(MONTHSL, y3, label='NP3')
		plt.plot(MONTHSL, y4, label='NP4')

		# Name the y axis 
		plt.ylabel('Average Electricity Consumption (kW)',fontsize=10)

		# Incline the x label and change the font size
		plt.xticks(rotation=45, ha='right', fontsize=10)
		
		# Name the x axis 
		#plt.xlabel('Month') 
		  
		# Giving a title to my graph 
		#plt.title('Average Electricity Consumption per Month in '+YEAR)

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('ElectricityConsumption_evaluation'+YEAR+'.png') 

		# Clear the figure
		plt.clf()

		#Plot tank level
		x = ['00:00', '01:00', '02:00', '03:00', '04:00' , '05:00', '06:00', '07:00', '08:00' ,'09:00' ,'10:00', '11:00', '12:00',
			'13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00']

		for (j, month) in enumerate(MONTHS):	
			y = average_tank_level[:,j]	
	
			# Plotting the points  
			plt.plot(x, y, label=MONTHSL[j])
			plt.xticks(rotation=90, ha='right', fontsize=10)

		f1.write('TK LEVEL: '+str(average_tank_level)+'\n')	

		plt.axhline(y=57, color='k', linestyle='--')
		
		plt.axhline(y=50, color='k', linestyle='--')

		# Naming the x axis 
		plt.xlabel('Time of Day') 
		# Naming the y axis 
		plt.ylabel('Average Level (m)') 
		  
		# Giving a title to the graph 
		#plt.title('Tank Level per Month in '+YEAR)

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('TankLevel_evaluation'+YEAR+'.png') 

		# Clear the figure
		plt.clf()

		print(average_tank_level)

		#Plot cycles in the pump operation
		y1 = average_cycles_month_np1
		y2 = average_cycles_month_np2
		y3 = average_cycles_month_np3
		y4 = average_cycles_month_np4

		f1.write('SW1: '+str(y1)+'\n')
		f1.write('SW2: '+str(y2)+'\n')
		f1.write('SW3: '+str(y3)+'\n')
		f1.write('SW4: '+str(y4)+'\n')
	
		# Plotting the points  
		plt.plot(MONTHSL, y1, label='NP1')
		plt.plot(MONTHSL, y2, label='NP2')
		plt.plot(MONTHSL, y3, label='NP3')
		plt.plot(MONTHSL, y4, label='NP4')

		# Naming the y axis 
		plt.ylabel('Average Switches ON/OFF')

		# Naming the x axis 
		#plt.xlabel('Month')

		# Incline the x label
		plt.xticks(rotation=45, ha='right', fontsize=10)

		# Giving a title to the graph 
		#plt.title('Average Switches ON/OFF Daily per Month in '+YEAR+'.png')

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('PumpSwitches_evaluation'+YEAR+'.png') 

		# Clear the figure
		plt.clf()

		f1.write('Cumulative Reward: '+str(R)+'\n')

		plt.plot(range(1, len(R) + 1), R, label='Reward')

		# Naming the x axis 
		plt.xlabel('Episodes') 
		# Naming the y axis 
		plt.ylabel('Cumulative Reward')

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('Cumulative_Reward.png') 

		# Clear the figure
		plt.clf()

		f1.close()
		f2.close()	
	
	def get_action(self):
		qs_list = self.model.predict(self.current_state)
		action = np.argmax(qs_list[0])
		print(action)
		return action

	def __init__(self):

		self.get_model()

		self.evaluate()

if __name__ == '__main__':
	obj = Evaluation()							
