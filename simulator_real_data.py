'''
Water distribution system simulator

AUTHORS: Henrique Donâncio, Laurent Vercouter and Harald Roclawski

Code related to the paper: The Pump Scheduling Problem: A Real-World Scenario for Reinforcement Learning

Submitted to the 36th Conference on Neural Information Processing Systems (NeurIPS 2022) Track on Datasets
and Benchmarks. Do not distribute.

First release: 08/06/2022

Source: https://gitlab.com/hdonancio/pumpscheduling

'''

import math
import datetime
import numpy as np
import pandas as pd
import tensorflow as tf
from operator import add
import rem # Module to train the model with REM

# The path where the data is stored
DATA_PATH = './Data/'
# STATE space dimension = (tank1_level_t, water_consumption , time of day (t), month, last_action, time_running, water quality)
# where tank level and demand of water at time t (in the format of ms) are continuous values; month and
# last action are one-hot encode, time_running discrete, and water quality binary.
STATE_SPACE = 25
# Size of expanded state (stacking states/observations)
STATE_SIZE = 4
# Data that will be loaded to feed the replay memory
YEARS = ['2013', '2014']
MONTHS = ['Januar', 'Februar', 'Maerz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']

# One-hot encoding for Months
MONTH_ENCODING = [[1,0,0,0,0,0,0,0,0,0,0,0], 
				  [0,1,0,0,0,0,0,0,0,0,0,0],
				  [0,0,1,0,0,0,0,0,0,0,0,0],
				  [0,0,0,1,0,0,0,0,0,0,0,0], 
				  [0,0,0,0,1,0,0,0,0,0,0,0],
				  [0,0,0,0,0,1,0,0,0,0,0,0],
				  [0,0,0,0,0,0,1,0,0,0,0,0],
				  [0,0,0,0,0,0,0,1,0,0,0,0],
				  [0,0,0,0,0,0,0,0,1,0,0,0],
				  [0,0,0,0,0,0,0,0,0,1,0,0],
				  [0,0,0,0,0,0,0,0,0,0,1,0],
				  [0,0,0,0,0,0,0,0,0,0,0,1]]

# Actions available
ACTIONS = [[0,0,0,0], [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]]

# To normalize the state's data
MIN_TK1 = 47
MAX_TK1 = 57
MIN_CONSUMPTION = 1
MAX_CONSUMPTION = 3240

class WormsSimulation:

	def generate_data(self):

		print('--------------------------------------------------------------------------------------------')
		print('#############################  PREPARING A NEW REPLAY MEMORY  ##############################')
		print('--------------------------------------------------------------------------------------------')	

		#---------------------------------------------------------------------------------------------#
		############################ VARIABLES OF THE WATER SYSTEM MODEL ##############################
		#---------------------------------------------------------------------------------------------#

		# DO NOT MODIFY THIS SECTION

		Q1 = [0.0, 320.0, 570.0, 670.0, 897.0, 1099.0, 1298.0, 1597.3, 1823.2]
		H1 = [76.81, 74.70, 71.61, 71.75, 72.29, 69.99, 67.95, 61.88, 55.79]
		ETA1 = [0.00, 38.60, 55.55, 60.68, 72.28, 78.85, 83.12, 85.96, 85.07]

		Q2 = [0.0, 240.0, 494.0, 694.0, 899.0, 1095.7, 1209.3, 1465.3]
		H2 = [74.15, 72.15, 70.78, 69.34, 66.44, 62.75, 60.12, 52.57]
		ETA2 = [0.00, 40.65, 63.22, 75.36, 83.02, 87.09, 88.32, 87.11]

		Q3 = [0.0, 237.0, 470.0, 645.0, 797.3, 1000.3, 1208.7, 1410.0] 
		H3 = [63.84, 62.46, 61.98, 60.61, 58.68, 54.87, 50.07, 43.83]
		ETA3 = [0.00, 38.28, 61.02, 72.36, 79.05, 84.94, 87.08, 84.91]

		Q4 = [0.0, 195.0, 440.0, 659.7, 806.3, 998.7, 1204.0]
		H4 = [64.13, 63.19, 61.62, 59.35, 56.61, 51.21, 43.71]
		ETA4 = [0.00, 41.95, 67.50, 80.96, 84.84, 86.04, 83.42]

		A = math.pi*math.pow(44,2)/4;
		rho = 1000
		g = 9.81
		p1 = -2.713E-09
		p2 = 0.000006504
		p3 = 4.384E-07
		p4 = -0.0001768

		Qanlage = np.linspace(0,2000,2000)
		p = np.polyfit(Q1,H1,2)
		H1 = np.polyval(p,Qanlage)
		pETA1 = np.polyfit(Q1,ETA1,2)
		ETA1 = np.polyval(pETA1,Qanlage)
		p = np.polyfit(Q2,H2,2)
		H2 = np.polyval(p,Qanlage)
		pETA2 = np.polyfit(Q2,ETA2,2)
		ETA2 = np.polyval(pETA2,Qanlage)
		p = np.polyfit(Q3,H3,2)
		H3 = np.polyval(p,Qanlage)
		pETA3 = np.polyfit(Q3,ETA3,2)
		ETA3 = np.polyval(pETA3,Qanlage)
		p = np.polyfit(Q4,H4,2)
		H4 = np.polyval(p,Qanlage)
		pETA4 = np.polyfit(Q4,ETA4,2)
		ETA4 = np.polyval(pETA4,Qanlage)

		###############################################################################################

		# Store the current state that will be introduced in the model for predictions
		self.current_state = np.zeros((1, STATE_SIZE, STATE_SPACE))
		self.expanded_state = []

		''' For pump operation control '''
		# Time Running is the time that some pump is operating; the last action is the latest pump chosed
		time_running = [0, 0, 0, 0, 0]
		last_action = 2

		HB = []
		# Tank level of the first timestep in real data 2013
		HB.append(53.22)

		# Store transitios in a text file
		f = open("replay_memory.txt", "w")

		for indexy, year in enumerate(YEARS):
			for indexm, month in enumerate(MONTHS):
				# Open dataframes
				data_time = pd.read_csv(DATA_PATH+year+'/WaterConsumption/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
				data_date = pd.read_csv(DATA_PATH+year+'/WaterConsumption/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['Date']]
				data_waterConsumption = pd.read_csv(DATA_PATH+year+'/WaterConsumption/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['Netzverbrauch_pval']]

				data_qr_np1 = pd.read_csv(DATA_PATH+year+'/NP/NP1/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_1_Volumenfluss_pval']]
				data_qr_np2 = pd.read_csv(DATA_PATH+year+'/NP/NP2/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_2_Volumenfluss_pval']]
				data_qr_np3 = pd.read_csv(DATA_PATH+year+'/NP/NP3/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_3_Volumenfluss_pval']]
				data_qr_np4 = pd.read_csv(DATA_PATH+year+'/NP/NP4/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_4_Volumenfluss_pval']]

				# Granularity of data in this csv file is higher than others. Exclude some lines to make it equal in timesteps
				data_time_np1_q = pd.read_csv(DATA_PATH+year+'/NP/NP1/Q/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
				for i, d in data_time_np1_q.iterrows():
					h,m,s = data_time_np1_q.iloc[i, 0].split(':')
					if(i == 0):
						h_i,m_i,s_i = data_time_np1_q.iloc[i, 0].split(':')
						# Reset the list of the tank levels
						HB_temp = HB[len(HB) - 1]
						HB = []
						HB.append(HB_temp)
					elif(m_i == m):
						data_qr_np1.drop([i], inplace=True)
					else: 
						m_i = m

				data_qr_np1.reset_index(drop=True, inplace=True)					

				# Fix duplicated data in Water Consumption
				data_time_waterConsumption = pd.read_csv(DATA_PATH+year+'/WaterConsumption/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
				for i, d in data_time_waterConsumption.iterrows():
					h,m,s = data_time_waterConsumption.iloc[i, 0].split(':')
					if i == 0:
						h_i,m_i,s_i = data_time_waterConsumption.iloc[i, 0].split(':')
					elif int(m) == 0:
						if h_i == h:
							j = 0
							while h_i == h:
								data_time.drop([i + j], inplace=True)
								data_date.drop([i + j], inplace=True)
								data_waterConsumption.drop([i + j], inplace=True)
								data_qr_np1.drop([i + j], inplace=True)
								data_qr_np2.drop([i + j], inplace=True)
								data_qr_np3.drop([i + j], inplace=True)
								data_qr_np4.drop([i + j], inplace=True)
								j += 1
								h,m,s = data_time_waterConsumption.iloc[i + j, 0].split(':')
						else:
							h_i = h

				# Reset indexes			
				data_time.reset_index(drop=True, inplace=True)
				data_date.reset_index(drop=True, inplace=True)
				data_waterConsumption.reset_index(drop=True, inplace=True)
				data_qr_np1.reset_index(drop=True, inplace=True)
				data_qr_np2.reset_index(drop=True, inplace=True)
				data_qr_np3.reset_index(drop=True, inplace=True)
				data_qr_np4.reset_index(drop=True, inplace=True)
						
				# Timesteps
				data_length = len(data_qr_np3)

				Hanlage = []
				QBP= np.zeros(data_length)
				HBP = np.zeros(data_length)
				etaBP = np.ones(data_length)

				for i in range(data_length - 1):

					if(pd.isnull(data_waterConsumption.iloc[i, 0]) or pd.isna(data_waterConsumption.iloc[i, 0]) or data_waterConsumption.iloc[i, 0] <= 0):
						data_waterConsumption.iloc[i, 0] = last_valid_waterConsumption		
					else:
						last_valid_waterConsumption = data_waterConsumption.iloc[i, 0]
					if(pd.isnull(data_waterConsumption.iloc[i+1, 0]) or pd.isna(data_waterConsumption.iloc[i+1, 0]) or data_waterConsumption.iloc[i+1, 0] <= 0):
						data_waterConsumption.iloc[i+1, 0] = last_valid_waterConsumption

					# Final state	
					if(data_time.iloc[i, 0] == 86399):
						final_state = True
					else: final_state = False		
				
					if(i == 0):	
						# Convert time HH:MM:SS to total seconds
						h,m,s = data_time.iloc[i, 0].split(':')
						data_time.iloc[i, 0] = int(datetime.timedelta(hours=int(h),minutes=int(m),seconds=int(s)).total_seconds())
						# Set water quality binary value
						water_quality = 0
						# Reset the list of the tank levels
						HB_temp = HB[len(HB) - 1]
						HB = []
						HB.append(HB_temp) 
						
					h,m,s = data_time.iloc[i+1, 0].split(':')
					data_time.iloc[i+1, 0] = int(datetime.timedelta(hours=int(h),minutes=int(m),seconds=int(s)).total_seconds())

					aa = p1 * data_waterConsumption.iloc[i, 0] + p2
					bb = p3 * data_waterConsumption.iloc[i, 0] + p4
					cc = HB[i]

					Qanlage_1 = list(map(lambda x:aa*pow(x,2),Qanlage))
					Qanlage_2 = list(map(lambda x:bb * x + cc,Qanlage))
					Hanlage.append(list(map(add, Qanlage_1, Qanlage_2)))

					if((pd.isnull(data_qr_np1.iloc[i, 0]) or pd.isna(data_qr_np1.iloc[i, 0])) and last_action == 1):
						action_index = 1
					elif((pd.isnull(data_qr_np2.iloc[i, 0]) or pd.isna(data_qr_np2.iloc[i, 0])) and last_action == 2):		
						action_index = 2
					elif((pd.isnull(data_qr_np3.iloc[i, 0]) or pd.isna(data_qr_np3.iloc[i, 0])) and last_action == 3):		
						action_index = 3
					elif((pd.isnull(data_qr_np4.iloc[i, 0]) or pd.isna(data_qr_np4.iloc[i, 0])) and last_action == 4):		
						action_index = 4
					elif(data_qr_np1.iloc[i, 0] != 0):
						action_index = 1
					elif(data_qr_np2.iloc[i, 0] != 0):
						action_index = 2
					elif(data_qr_np3.iloc[i, 0] != 0):
						action_index = 3
					elif(data_qr_np4.iloc[i, 0] != 0):
						action_index = 4			
					else: action_index = 0	

					# Normalize data for the state
					tank_level = (HB[i] - MIN_TK1)/(MAX_TK1 - MIN_TK1)
					water_consumption =  (data_waterConsumption.iloc[i, 0] - MIN_CONSUMPTION)/(MAX_CONSUMPTION - MIN_CONSUMPTION)
					current_time = (data_time.iloc[i, 0] - 59)/(86399 - 59)
					time_running_normalized = [time/1440 for time in time_running]

					# Create a new state
					state = [tank_level, water_consumption, current_time] + MONTH_ENCODING[indexm]+ ACTIONS[last_action] + time_running_normalized + [water_quality]
					self.expanded_state.append(state)

					# Shift (drop) samples in the expanded state
					if i < STATE_SIZE - 1:
						action_index = last_action
					else:	
						if i == STATE_SIZE - 1:
							for j in range(STATE_SIZE):
								self.current_state[0][j] = self.expanded_state[j] 
						elif i >= STATE_SIZE:
							self.expanded_state.pop(0)
							for j in range(STATE_SIZE):
								self.current_state[0][j] = self.expanded_state[j]	

					if(action_index > 0):
						if(action_index == 1):
							HPumpe = H1
							ETAPumpe = pETA1
						elif(action_index == 2):
							HPumpe = H2
							ETAPumpe = pETA2
						elif(action_index == 3):
							HPumpe = H3
							ETAPumpe = pETA3
						elif(action_index == 4):
							HPumpe = H4
							ETAPumpe = pETA4

						res = np.argmin(abs(Hanlage[i]-HPumpe))	

						QBP[i] = Qanlage[res]
						HBP[i] = HPumpe[res]
						etaBP[i] = np.polyval(ETAPumpe, QBP[i])
					else: 
						QBP[i] = 0
						HBP[i] = 0
						etaBP[i] = 1
						
					# Tank level
					HB_i = 	HB[i]+(QBP[i]-data_waterConsumption.iloc[i, 0])/60/A
					if HB_i < 47:
						HB.append(47)
					elif HB_i > 57:
						HB.append(57)
					else:
						HB.append(HB_i)

					# Electricity consumption
					pbp = ((rho*g*QBP[i]/3600*HBP[i])/55*etaBP[i])/1000

					# Cumulative time that pumps are operating
					time_running[action_index] += 1	

					###############################################################################################
					####################################### REWARD FUNCTION #######################################	

					# Check the previous action and the current action, update and give a penalty if the case
					# Rule for Reward Q/Kw
					#if(action_index == last_action or action_index == 0 or time_running[action_index] == 0):
					#	p = 1
					# Rule for Reward Kw
					if action_index == last_action or time_running[action_index] == 0:
						p = 1 	
					else:
						p = 30	

					p += time_running[action_index]

					# Check the constraints of the tank level and give a penalty if the case
					if HB_i < 50:
						c = abs(HB_i - 50)
					elif HB_i >= 57:
						c = 1
					else:
						c = 0

					# Clip the penalty value
					if c > 1 : c = 1

					# Check if water tank level decrease below to determined level for water quality
					if water_quality == 0 and HB[i] >= 50 and HB[i] < 53:
						c = -1
						water_quality = 1

					# *** Reward Function Q/Kw *** #	
					# if action_index > 0:
					# 	reward = math.exp(1/(-QBP[i]/etaBP[i])) - c*10 + math.log(1/p)
					# else:
					# 	reward = -c*10 + math.log(1/p)

					# Clip the reward value
					#reward = (reward - (-18))/(11 - (-18))

					# *** Reward Function Kw *** #
					if action_index > 0:
						reward = -math.exp(-1/etaBP[i]) - c*10 + math.log(1/p)
					else:
						reward = -c*10 + math.log(1/p)
					# Normalize the reward value
					reward = (reward - (-20))/(10 - (-20))					
					###############################################################################################
					
					# Normalize data
					next_tank_level = (HB[i+1] - MIN_TK1)/(MAX_TK1 - MIN_TK1)
					next_water_consumption =  (data_waterConsumption.iloc[i+1, 0] - MIN_CONSUMPTION)/(MAX_CONSUMPTION - MIN_CONSUMPTION)
					next_current_time = (data_time.iloc[i+1, 0] - 59)/(86399 - 59)
					time_running_normalized[action_index] = time_running[action_index]/1440

					# Create the next state
					next_state = [next_tank_level, next_water_consumption, next_current_time] + MONTH_ENCODING[indexm]+ ACTIONS[action_index] + time_running_normalized	+ [water_quality]

					# Update the last action performed
					last_action = action_index

					# Action performed
					action = ACTIONS[action_index]

					# Check if the time step is the last of the day
					if(data_time.iloc[i+1, 0] == 86399):
						done = True
						time_running = [0, 0, 0 ,0 ,0]
						water_quality = 0
					else:
						done = False

					features = (state, action, next_state, reward, done)

					# Write the experience in a txt file	 	
					f.write(str(features)+'\n')

	def __init__(self):

		self.drqn = rem.REM('simulator')

		#self.get_model()

		self.generate_data()

		self.drqn.feed_memory()
		print('Feeding the replay memory ...\n')

		self.drqn.train_model()

if __name__ == '__main__':
	obj = WormsSimulation()							
