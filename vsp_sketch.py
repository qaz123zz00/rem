"""

This class is adapted & modified from the code written by Christian Geil (2014) Drehzahlregelung_Kennlinienberechnung_v03c.m

It represents a starting point to extend the actions to variable speed pumps

""" 

import matplotlib.pyplot as plt
import math
import numpy as np
from operator import add
from scipy.interpolate import UnivariateSpline
from scipy.optimize import fmin_slsqp

n = np.array([2000, 2500, 3000, 3500, 4000, 4500])
Nn = 4000.0

Q2 = [0.0, 240.0, 494.0, 694.0, 899.0, 1095.7, 1209.3, 1465.3]
H2 = [74.15, 72.15, 70.78, 69.34, 66.44, 62.75, 60.12, 52.57]
ETA2 = [0.00, 40.65, 63.22, 75.36, 83.02, 87.09, 88.32, 87.11]

ETA2 = np.divide(ETA2, 100)

#Considerint pump NP2
HQ_Nn_Fkt = UnivariateSpline(Q2, H2)
etaQ_Nn_Fkt = UnivariateSpline(Q2, ETA2);

Q_Nn_Opt = fmin_slsqp(lambda x: -etaQ_Nn_Fkt(x), (np.max(Q2)-np.min(Q2))/2)

H_Nn_Opt = HQ_Nn_Fkt(Q_Nn_Opt)
eta_Nn_Opt = etaQ_Nn_Fkt(Q_Nn_Opt)

Q_n_Opt = n/Nn * Q_Nn_Opt;
H_n_Opt = np.power((n/Nn), 2) * H_Nn_Opt 

eta_n_Opt = 1 - (1-eta_Nn_Opt) * np.power(np.divide(Nn,n), 0.1)

HHopt_Nn_Basis = np.divide(H2, H_Nn_Opt)
Q1Qopt_Nn_Basis = np.divide(Q2, Q_Nn_Opt)
QQopt_step = 0.02;
min_QQopt = np.min(Q1Qopt_Nn_Basis);
min_QQopt = min_QQopt + (QQopt_step - (min_QQopt//QQopt_step))
max_QQopt = 1.4001;
QQopt = np.arange(min_QQopt, max_QQopt, QQopt_step, dtype=float)
QQopt = np.concatenate((np.array([0.0]),QQopt))

HHopt_QQopt_Fkt = UnivariateSpline(Q1Qopt_Nn_Basis, HHopt_Nn_Basis)
poly_Koeff1 = np.polyfit(Q1Qopt_Nn_Basis[0:2], HHopt_Nn_Basis[0:2], 2)
poly_Koeff2 = np.polyfit(Q1Qopt_Nn_Basis[-3:-1], HHopt_Nn_Basis[-3:-1], 2)
min_Q1Qopt_Nn_Basis = np.min(Q1Qopt_Nn_Basis)
max_Q1Qopt_Nn_Basis = np.max(Q1Qopt_Nn_Basis)

HHopt_QQopt = np.zeros(len(QQopt))

for i in range(0, len(QQopt)):
	if QQopt[i] < min_Q1Qopt_Nn_Basis:
		HHopt_QQopt[i] = np.polyval(poly_Koeff1, QQopt[i])
	elif QQopt[i] < max_Q1Qopt_Nn_Basis:
		HHopt_QQopt[i] = HHopt_QQopt_Fkt(QQopt[i])
	else: 
		HHopt_QQopt[i] = np.polyval(poly_Koeff2, QQopt[i])

EtaETAopt_Nn_Basis = np.divide(ETA2, eta_Nn_Opt)
EtaETAopt_Nn_Basis = np.concatenate((np.array([0.0]), EtaETAopt_Nn_Basis))
Q2Qopt_Nn_Basis = np.divide(Q2, Q_Nn_Opt)
Q2Qopt_Nn_Basis = np.concatenate((np.array([0.0]), Q2Qopt_Nn_Basis))
poly_Koeff = np.array([])
poly_Koeff = np.polyfit(Q2Qopt_Nn_Basis, EtaETAopt_Nn_Basis, 2)
EtaETAopt_QQopt = np.polyval(poly_Koeff, QQopt)
EtaETAopt_QQopt[0] = 0.0

for i in range(0, len(n)):
	Q_n = Q_n_Opt[i] * np.transpose(QQopt)
	H_n = H_n_Opt[i] * HHopt_QQopt
	H_n = np.transpose(H_n)
	ETA = eta_n_Opt[i] * EtaETAopt_QQopt
	ETA = np.transpose(ETA2)
	if (i > 1):
		Q = np.concatenate((Q,Q_n))
		H = np.concatenate((H,H_n))
		ETA = np.concatenate((ETA,ETA2))
	else:
		Q = Q_n
		H = H_n
		ETA = ETA2
